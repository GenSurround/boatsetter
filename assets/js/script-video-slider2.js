var $screen_xs = 480,
    $screen_sm = 768,
    $screen_md = 992,
    $screen_lg = 1200,
    video = true;


// for default-alt
var standardHeight = $('#form-standard-search .toggle-form').height();


$(document).ready(function() {

    if (window.location.href.indexOf("#login") >= 0) {
        $('#bs-modal-login').modal('show');
    }

    $('a[data-find]').on('click', function(event) {
        event.preventDefault();

        var origin = $(this).data('origin'),
            find = $(this).data('find');

        $('#' + origin).toggleClass('hidden');
        $('#' + find).toggleClass('hidden');

        if (jQuery('.flexslider-fullwidth'))
            resizeHomeSlideshow();
    });

    //  $( '.nav-tabs' ).tabCollapse();

    $('#filter').stick_in_parent();


    $('input[type=date]').each(function(index, input) {
        var $input = $(input);

        $(input).css('z-index', 1050);

        $input.attr('type', 'text').datepicker({
            beforeShow: function(input, inst) {
                var widget = $(inst).datepicker('widget');

                widget.css({
                    'margin-left': $(input).parent().outerWidth(true) - widget.outerWidth(true)
                });
                // console.log(widget.zIndex());
            }
        });
    });

    $(".password-tip .icon").click(function() {
        event.preventDefault();
        $(".password-tip").tooltip('toggle');
    });

    $(".tip .icon").click(function() {
        event.preventDefault();
        $(".tip").tooltip('toggle');
    });


    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });

    $('input[type=range]').each(function(index, input) {
        var $input = $(input),
            $range = $input.attr('data-range'),
            $min = parseInt($input.attr('data-min'), 10) || 0,
            $max = parseInt($input.attr('data-max'), 10) || 100,
            $value = parseInt($input.attr('value'), 10) || 0,
            $step = parseInt($input.attr('data-step'), 10) || 1;

        if (typeof $range !== typeof undefined && $range == 'true') {
            var $name = $input.attr('name');

            $slider = $('<div />').slider({
                range: true,
                min: $min,
                max: $max,
                values: [$min, $max],
                step: $step,
                slide: function(event, ui) {
                    $input.siblings('.range-label').text("$" + ui.values[0] + " - $" + ui.values[1]);

                    $(input).siblings('input[name="' + $name + '-min"]').val(ui.values[0]);
                    $(input).siblings('input[name="' + $name + '-max"]').val(ui.values[1]);
                },
            });

        } else {
            $slider = $('<div />').slider({
                range: $range,
                min: $min,
                max: $max,
                value: $value,
                step: $step,
            });

        } // endif

        $input.after($slider).hide();
    });



    $('.dropdown-form').on('click', function(event) {
        event.stopPropagation();
    });

    $('.dropdown-menu a[data-toggle="tab"]').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        $(this).tab('show');
    });


    $('#top-destinations').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: false,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 5,
                slidesToScroll: 5,
                infinite: true,
                dots: false
            }
        }]

    });


    $('#boats-overview').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: false,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                dots: false
            }
        }]

    });

    $('#new-captain-slider').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: true,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        // autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }]

    });

    var toggle_newCaptain = $('#bs-new-captain-toggle'),
        newCaptainPanel = $('#bs-new-captain');

    newCaptainPanel.on('shown.bs.collapse', function() {
        toggle_newCaptain.addClass('btn-knockout');
        toggle_newCaptain.blur();
    });
    newCaptainPanel.on('hidden.bs.collapse', function() {
        toggle_newCaptain.removeClass('btn-knockout');
    });

    toggle_newCaptain.trigger("click");




    $('#new-boat-slider').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: true,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        // autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }]

    });

    var toggle_newBoat = $('#bs-new-boat-toggle'),
        newBoat = $('#bs-new-boat');

    newBoat.on('shown.bs.collapse', function() {
        toggle_newBoat.addClass('btn-knockout');
        toggle_newBoat.blur();
    });
    newBoat.on('hidden.bs.collapse', function() {
        toggle_newBoat.removeClass('btn-knockout');
    });

    toggle_newBoat.trigger("click");


    $('#lyb-images-slider').slick({
        arrows: false,
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: true,
        centerPadding: '0px',
        infinite: true,
        dots: true,
        // autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }]

    });



    $('#related-gallery').slick({
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        centerMode: false,
        centerPadding: '0px',
        infinite: true,
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                arrows: true,
                dots: true
            }
        }]

    });








    $('#boat-details-gallery').slick({
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode: false,
        centerPadding: '0px',
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#boat-details-nav',
        responsive: [{
            breakpoint: $screen_md,
            settings: {
                fade: true,
                dots: false
            }
        }]
    });


    $('#boat-details-nav').slick({
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        centerPadding: '0px',
        focusOnSelect: true,
        asNavFor: '#boat-details-gallery',
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                arrows: true,
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5,
            }
        }]
    });


    //
    // CAPTAIN PROFILE SLIDERS

    $('#captain-details-gallery').slick({
        customPaging: function(slider, i) {
            return '<a href="#slide-' + (i + 1) + '" data-role="none"></a>';
        },
        mobileFirst: true,
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode: false,
        centerPadding: '0px',
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#captain-details-nav',
        responsive: [{
            breakpoint: $screen_md,
            settings: {
                fade: true,
                dots: false
            }
        }]
    });


    $('#captain-details-nav').slick({
        prevArrow: '<a href="#prev" data-role="none" class="slick-prev" aria-label="previous"><em class="fa fa-angle-left"></em></a>',
        nextArrow: '<a href="#next" data-role="none" class="slick-next" aria-label="next"><em class="fa fa-angle-right"></em></a>',
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        centerPadding: '0px',
        focusOnSelect: true,
        asNavFor: '#captain-details-gallery',
        responsive: [{
            breakpoint: $screen_sm,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                arrows: true,
            }
        }, {
            breakpoint: $screen_md,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
            }
        }, {
            breakpoint: $screen_lg,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5,
            }
        }]
    });

    //
    // END CAPTAIN PROFILE SLIDERS

    var homeSlideshowActive = false;

    if ($('.flexslider').length > 0) {
        resizeHomeSlideshow();
    }


    // For Multiple Modals: Include data-second-modal="true" on html markup
    // $('*[data-second-modal="true"]').on('hidden.bs.modal', function() {
    $('.modal').on('hidden.bs.modal', function() {
        if ($('.modal-backdrop').is(":visible"))
            $('body').addClass('modal-open');
    });
    // For Multiple Modals: Include data-second-modal="true" on html markup


    // TOP NOTIFICATION: Include data-top-notification="true" on html markup
    window.setTimeout(function() {
        $('*[data-top-notification="true"]').collapse('show');
    }, 1000);
    window.setTimeout(function() {
        $('*[data-top-notification="autohide"]').collapse('show');
    }, 1000);
    if ($('*[data-top-notification="autohide"]').length > 0) {
        window.setTimeout(function() {
            $('*[data-top-notification="autohide"]').collapse('hide');
        }, 10000);
    }
    // TOP NOTIFICATION: Include data-top-notification="true" on html markup


    $('.bs-notifications-mobile .dropdown-toggle').click(function(event) {
        if ($('.bs-navbar-mobile.in').length > 0) {
            console.log('OPEN');
            $('#bs-navbar-mobile').collapse('hide');
        }
    });

    $('#sticky-map').parent().css({
        'height': $('#sticky-map-parent').height() - 79
    });
    $('#sticky-map').stick_in_parent();

    $('#bs-insurance-submit').click(function(event) {
        event.preventDefault();
        verifyInsurance();
    });

    $('a.only-toggle').click(function(e) {
        e.preventDefault();
    });


    $('#search-main').stick_in_parent();

    // FIX NOTIFICATION BELL Z-INDEX
    var boatOverview = $('#boat-overview'),
        bell = $('.bs-notifications-mobile');

    boatOverview.on('show.bs.modal', function() {
        bell.css('z-index', 2);
        // console.log("WAH THAWTHAW");
    });

    boatOverview.on('hidden.bs.modal', function() {
        bell.css('z-index', 4);
        // console.log("aidsoaisdoi");
    });

    // FIX NOTIFICATION BELL Z-INDEX // END



    // FORCE RESIZE TO FIX SLIDER ISSUES ON IE
    $(window).resize();
    // FORCE RESIZE TO FIX SLIDER ISSUES ON IE // END




});




$(window).resize(function() {

    var width = $(window).width(),
        height = $(window).height();


    if ($('.flexslider').length > 0) {
        resizeHomeSlideshow();
    }

    if ($screen_sm > width) {
        $('#filter').trigger('sticky_kit:detach');
        $('.lyb-list').removeClass('in');


    } else {
        $('#filter').stick_in_parent();
        $('.lyb-list').addClass('in');
    } // endif


});

function resizeHomeSlideshow() {

    var wWidth = $(window).width(),
        wHeight = $(window).height(),
        limit = 600,
        video_limit = 600,
        about = ($('.video-wrapper').hasClass('about')) ? true : false;


    // default-alt slider
    // ------------------------------------------
    if (wWidth >= $screen_sm) { // DESKTOP


        var tHeight = $('.slideshow-overlay').outerHeight(true),
            dHeight = wHeight - $("#what-how-overview").outerHeight(true) - 65;
        if (video) {
            dHeight = wHeight - $("#search-main").outerHeight(true) - 65;
        }
        dHeight = (dHeight > limit) ? limit : dHeight;
        dHeight = (about) ? (wWidth / 1.77 ) : dHeight; //2.394285714


        $('.slideshow-overlay, .flexslider-fullwidth .slides > li').css({
            position: 'absolute'
                // ,property2: 'value2'
        });
        $('.flexslider-fullwidth').css({
            position: 'relative'
                // ,property2: 'value2'
        });

        if (dHeight > 400 || about) {
            $('.flexslider-fullwidth, .flexslider-fullwidth .slides > li, .slider-row').css('height', dHeight);
            $('.slideshow-overlay').css({
                'margin-top': 0,
                'top': "40px" //"calc( 50% - " + (tHeight / 2) + "px )"
            });
        }

        $('.flexslider-fullwidth .slides').click(function(event) {
            event.preventDefault();
        });

        homeSlideshowActive = activateHomeSlideshow();
    } else { // MOBILE

        var landscapeDiff = 0;
        if (wHeight < wWidth) {
            landscapeDiff = 125;
        }

        var mHeight = wHeight + landscapeDiff;

        $('.slideshow-overlay').css({
            position: 'relative'
        });


        $('.flexslider-fullwidth').css({
            position: (about) ? 'relative' : 'absolute'
        });

        // standardHeight = (about) ? (wHeight / 4) : standardHeight;

        if (!about) {
            $('.flexslider-fullwidth, .flexslider-fullwidth .slides > li').css('height', mHeight - standardHeight);
        } else {
            $('.flexslider-fullwidth, .flexslider-fullwidth .slides > li').css('height', wWidth / 1.33);
        }


        $('.slideshow-overlay').css({
            'margin-top': mHeight - standardHeight - 20,
            'top': 0
        });

        homeSlideshowActive = activateHomeSlideshow();
    }
}

function activateHomeSlideshow() {

    var wWidth = $(window).width(),
        wHeight = $(window).height(),
        imgUrl,
        flex = $('.flexslider'),
        videoWrapper = $('.video-wrapper'),
        videoInner = $('.video-wrapper video');


    if (!video || wWidth <= $screen_sm) { // SLIDER

        $('.flexslider .slide-image').each(function(index, el) {

            if (wWidth > $screen_sm)
                imgUrl = 'url(' + $(this).data('img-sm') + ')';
            else
                imgUrl = 'url(' + $(this).data('img-xs') + ')';

            $(this).css('background-image', imgUrl);

        });

        videoWrapper.css('display', 'none');

        jQuery('.flexslider').flexslider({
            controlNav: true,

            pauseText: '<i class="fa fa-pause fa-3x"></i>',
            playText: '<i class="fa fa-play fa-3x"></i>',
            prevText: '<i class="fa fa-angle-left fa-3x"></i>',
            nextText: '<i class="fa fa-angle-right fa-3x"></i>',
            directionNav: true,

            slideshow: true,
            animationSpeed: 600,
            start: function(slider) {
                // alert( 'test' );
                jQuery('.flex-active-slide').find('.caption').fadeIn(300);
            },
            before: function(slider) {
                jQuery('.flex-active-slide').find('.caption').fadeOut(200);
            },
            after: function(slider) {
                jQuery('.flex-active-slide').find('.caption').delay(200).fadeIn(300);
            }
        });

        $(".slides").css('opacity', '1');
        $(".flex-control-nav").css('display', 'block');
        $(".flex-control-paging").css('display', 'block');
        $(".flex-direction-nav").css('display', 'block');
    } else { // VIDEO

        videoWrapper.css('display', 'block');

        if (flex.width() / flex.height() > 2.147651007) {
            videoInner.css('width', "100%");
            videoInner.css('height', "auto");
        } else {
            videoInner.css('height', "100%");
            videoInner.css('width', "auto");
        }

        $(".slides").css('opacity', '0');
        $(".home-toggle-form.bg-dark-blue").css('background-color', 'rgba(0, 104, 176, 0.8)');


        $(".flex-control-nav").css('display', 'none');
        $(".flex-control-paging").css('display', 'none');
        $(".flex-direction-nav").css('display', 'none');
    }



    return true;
}

function verifyInsurance() {

    var questions = $('#bs-insurance-form input:radio');
    var need_captain = false;

    questions.each(function(index, el) {
        if (el.checked && el.value == 'yes') {

            need_captain = true;
        }
    });

    if (need_captain) {
        $('#bs-modal-need-captain').modal('toggle');
        $('#captain-required-alert').addClass('in');
    }
}

// FORCE RESIZE TO FIX SLIDER ISSUES ON IE
$(window).resize();
// FORCE RESIZE TO FIX SLIDER ISSUES ON IE // END
