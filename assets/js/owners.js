var controller_owners = [],
    controller_icons = [],
    controller_reasons = [];


for (i = 0; i <= $('.panel-owners').length; i++) {
    controller_owners[i] = new ScrollMagic.Controller({
        globalSceneOptions: {
            duration: 2000,
            offset: (50 * (i % 4)) - 150
        }
    });

    new ScrollMagic.Scene({
            triggerElement: '.panel-owners-' + (i + 1)
        })
        .setClassToggle('.panel-owners-' + (i + 1), "active") // add class toggle
        .addTo(controller_owners[i]);
}

for (i = 0; i <= $('.about-icon').length; i++) {
    controller_icons[i] = new ScrollMagic.Controller({
        globalSceneOptions: {
            duration: 3000,
            offset: (50 * (i % 4)) - 150
        }
    });

    new ScrollMagic.Scene({
            triggerElement: '.about-icon-' + (i + 1)
        })
        .setClassToggle('.about-icon-' + (i + 1), "active") // add class toggle
        .addTo(controller_icons[i]);
}

for (i = 0; i <= $('.reasons-icon').length; i++) {
    controller_reasons[i] = new ScrollMagic.Controller({
        globalSceneOptions: {
            duration: 3000,
            offset: (50 * (i % 4)) - 150
        }
    });

    new ScrollMagic.Scene({
            triggerElement: '.reasons-icon-' + (i + 1)
        })
        .setClassToggle('.reasons-icon-' + (i + 1), "active") // add class toggle
        .addTo(controller_reasons[i]);
}


var earningCalculator = [
        [
            [300, 400],
            [400, 500],
            [500, 800],
            [800, 1200]
        ],
        [
            [500, 600],
            [600, 800],
            [700, 1100],
            [1000, 1800]
        ],
        [
            [700, 800],
            [900, 1200],
            [900, 1300],
            [1300, 2000]
        ]
    ],
    charterDuration = 0,
    boatLength = 0,
    tripsMonth = 0,
    estimateEarnings = 0;


// With JQuery
$('#ex1').slider({
    formatter: function(value) {
        tripsMonth = value;
        refreshEarnings()
        return value;
    }
    , tooltip: 'always'
});

// Without JQuery
var slider = new Slider('#ex1', {
    formatter: function(value) {
        tripsMonth = value;
        refreshEarnings();
        return value;
    }
    , tooltip: 'always'
});

$(".charter-duration").click(function(event) {
    charterDuration = $(this).attr('data-value');
    $('#charter-duration .inner p').text($(this).text());
    $('#charter-duration').collapse();
    refreshEarnings();
});

$(".boat-length").click(function(event) {
    boatLength = $(this).attr('data-value');
    $('#boat-length .inner p').text($(this).text());
    $('#boat-length').collapse();
    refreshEarnings();
});

if ($(window).width() < $screen_sm) {
    $('#charter-duration').collapse();
    $('#boat-length').collapse();
}


refreshEarnings();


function refreshEarnings() {
    if ((charterDuration * boatLength * tripsMonth) == 0) {
        estimateEarnings = "---";
    } else {
        estimateEarnings = "$" + (tripsMonth * earningCalculator[charterDuration - 1][boatLength - 1][0]) + " - $" + (tripsMonth * earningCalculator[charterDuration - 1][boatLength - 1][1]);
        // estimateEarnings = "<span class='small'>from:</span> $" + (tripsMonth * earningCalculator[charterDuration - 1][boatLength - 1][0]) + "<br><br><span class='small'>to:</span> $" + (tripsMonth * earningCalculator[charterDuration - 1][boatLength - 1][1])
    }
    $('#estimate-earnings').html(estimateEarnings);
}
